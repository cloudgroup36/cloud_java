import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Scoring {

	public static void main(String[] args) {
		List<String[]> scores = readScores();
		Map<String,Double>unsorted = scoring(scores);
		String sentiment = "";
		Double max = 0.0;
		for(String key: unsorted.keySet() ){
			System.out.println("Score: "+key+","+unsorted.get(key));
			if(unsorted.get(key) >max){
				sentiment = key;
				max = unsorted.get(key);
			}
		}
		System.out.println();
		System.out.println("This sentence is classified to: "+sentiment+" with "+max );
		
	}

	public static List<String[]> readScores() {
		String csvFile ="scores.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		List<String[]> scores = new ArrayList<String[]>();
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] country = line.split(cvsSplitBy);
				if (country[0].equals("word")){
					continue;
				}
				scores.add(country);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return scores;
	}

	public static Map<String,Double> scoring(List<String[]> scores) {
		String l = "💛💚💚💚💚 proud happy speakup 💔";
		String[] ls = l.split("[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");
		List<String> temp = new ArrayList<String>();
		for (int i = 0; i < ls.length; i++) {
			if (isEmoicon(ls[i]) && ls[i].length() > 2) {
				String templist = ls[i].replaceAll(".(?!$)", "$0 ");
				String[] list = templist.split(" ");
				for (int j = 0; j < list.length; j++) {
					temp.add(list[j]);
				}
			} else {
				temp.add(ls[i].toLowerCase());
			}
		}
		
		Map<String,Double> score = new LinkedHashMap<String,Double>();
		score.put("Affection",0.0);
		score.put("Cheerfulness",0.0);
		score.put("Optimism",0.0);
		score.put("Astonishment",0.0);
		score.put("Embarrasment",0.0);
		score.put("Outrage",0.0);
		score.put("Disgust",0.0);
		score.put("Depressed",0.0);
		score.put("Worry",0.0);
		for (int i = 0; i < temp.size(); i++) {
			for (int j = 0; j < scores.size(); j++) {
				if (temp.get(i).equals(scores.get(j)[0])) {
//					System.out.println("Found match: "+temp.get(i));
					int m = 0;
					for (Map.Entry<String,Double> thisScore: score.entrySet()){
						thisScore.setValue(thisScore.getValue()+Double.parseDouble(scores.get(j)[m + 1]));
						m++;
					}
				}
			}
		}
		
		return score;

	}
	

	public static boolean isEmoicon(String temp) {
		String emoicon = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee]*)";
		if (temp.matches(emoicon) && temp.length() % 2 == 0) {
			return true;
		}
		return false;
	}

}
