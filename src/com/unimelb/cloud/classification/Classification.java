package com.unimelb.cloud.classification;
import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.Filter;
import  weka.classifiers.*;
import weka.classifiers.trees.J48;

import java.io.*;
import java.util.*;

import com.unimelb.cloud.config.Config;
import com.unimelb.cloud.preproccess.DicLoader;
import com.unimelb.cloud.util.Util;
public class Classification {
	public static void main(String[] args) throws Exception {
		// d://data/output/J48.model d://data/output/firstTime.txt meat potato salad
		//d://data/output/J48.model d://data/output/firstTime.txt d://data/output/FoodDic3Sorted.txt beer
		//loadProcess();
		//System.out.println(args.length);
		if(args.length>=4){
			String modelFilePath = args[0];
			String outputPath = args[1];
			String dicPath=args[2];
			String[] words=new String[args.length-3];
			for(int i=3;i<args.length;i++){
				words[i-3]=args[i];
			}
			String line=predict(words,modelFilePath,outputPath,dicPath);
			System.out.println(line);
			
		}
		
		
		//loadProcess();
	}
	
	public static String predict(String[] words,String modelFilePath,String outputPath,String dicPath){
		DicLoader dl=new DicLoader(dicPath);
		dl.loadDic();
		//4.Sentence assign to dic and return a 0101010 list
		//System.out.println(dl.DIC);
		StringBuilder sb=new StringBuilder();
		sb.append("?");
		int[] arr=new int[dl.DIC.length];
		boolean flag = false;
		for(int i=0;i<dl.DIC.length;i++){
			sb.append(',');
			for(int j=0;j<words.length;j++){
				if(words[j].equals(dl.DIC[i])){
					arr[i]=1;
					flag=true;
				}
			}
			sb.append(arr[i]);
		}
		if(flag==false){
			//Write "No result"
			return null;
		}
		
		
		try {
			File f1=new File("./temp1.arff");
			 File f2=new File("./temp2.arff");
			 File f3=new File(outputPath);
			 f1.delete();
			 f2.delete();
			 f3.delete();
			//System.out.println(sb.toString());
			Util.append2File("./temp1.arff", Util.templateStr);
			Util.append2File("./temp1.arff", sb.toString());
			J48 j48 = new J48();
			j48 = (J48)weka.core.SerializationHelper.read(modelFilePath);
			// load unlabeled data
			
			 Instances unlabeled = new Instances(new BufferedReader(new FileReader("./temp1.arff")));
			 // set class attribute unlabeled.numAttributes() - 1
			 unlabeled.setClassIndex(0);
			 
			 // create copy
			 Instances labeled = new Instances(unlabeled);
			 
			 // label instances
			 for (int i = 0; i < unlabeled.numInstances(); i++) {
			   double clsLabel = j48.classifyInstance(unlabeled.instance(i));
			   labeled.instance(i).setClassValue(clsLabel);
			 }
			 // save labeled data
			 BufferedWriter writer = new BufferedWriter(
			                           new FileWriter("./temp2.arff"));
			 writer.write(labeled.toString());
			 writer.newLine();
			 writer.flush();
			 writer.close();
			 String line=Util.getLastXLine("./temp2.arff", 1);
			 //System.out.println(line);
			 
			 f1.delete();
			 f2.delete();
			 f3.delete();
			 String sentiment = line.split(",")[0];
			 Util.append2File(outputPath,sentiment );
			 //System.out.println(sentiment);
			 return sentiment;
		} catch (Exception ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
		return null;
	}
	
	public static void train() throws Exception{
		 DataSource source = new DataSource(Config.trainingFile);
		 Instances data = source.getDataSet();
		 // setting class attribute if the data format does not provide this information
		 // For example, the XRFF format saves the class attribute information as well
		 if (data.classIndex() == -1)
		   data.setClassIndex(1);
		 String[] options = new String[2];
		 options[0] = "-R";                                    // "range"
		 options[1] = "1";                                     // first attribute
		 Remove remove = new Remove();                         // new instance of filter
		 remove.setOptions(options);                           // set options
		 remove.setInputFormat(data);                          // inform filter about dataset **AFTER** setting options
		 Instances newData = Filter.useFilter(data, remove);   // apply filter
		 J48 j48 = new J48();
	      j48.buildClassifier(newData);
	      Evaluation eval = new Evaluation(newData);
	      eval.crossValidateModel(j48, newData, 10 , new Random(1));

	      System.out.println(eval.toSummaryString("\n Results \n=====\n",true));
	      System.out.println(eval.fMeasure(1)+" "+eval.precision(1)+" "+eval.recall(1)+" "); 
	      
	      weka.core.SerializationHelper.write(Config.modelFile, j48);
	}
	
	public static void loadProcess() throws Exception{
		J48 j48 = new J48();
		j48 = (J48)weka.core.SerializationHelper.read(Config.modelFile);
		// load unlabeled data
		
		 Instances unlabeled = new Instances(new BufferedReader(new FileReader(Config.testFile)));
		 // set class attribute unlabeled.numAttributes() - 1
		 unlabeled.setClassIndex(0);
		 
		 // create copy
		 Instances labeled = new Instances(unlabeled);
		 
		 // label instances
		 for (int i = 0; i < unlabeled.numInstances(); i++) {
		   double clsLabel = j48.classifyInstance(unlabeled.instance(i));
		   labeled.instance(i).setClassValue(clsLabel);
		 }
		 // save labeled data
		 BufferedWriter writer = new BufferedWriter(
		                           new FileWriter(Config.testFileOutput));
		 writer.write(labeled.toString());
		 writer.newLine();
		 writer.flush();
		 writer.close();
	}
	

}
