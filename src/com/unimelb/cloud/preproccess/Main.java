package com.unimelb.cloud.preproccess;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import net.sf.json.JSONObject;

import com.unimelb.cloud.config.Config;
import com.unimelb.cloud.dao.Tweet;
import com.unimelb.cloud.sentiment.analysis.Scoring;
import com.unimelb.cloud.util.Util;

public class Main {

	public static void main(String[] args) throws Exception {
		//runCommon();
		//runMedia();
		runLocation();
	}
	
	
	
	public static void runLocation() throws Exception{
		long start = System.currentTimeMillis();
		BufferedReader br = new BufferedReader(new FileReader(Config.locationInputTwetter));//data/bigTwitter.json
		int count=0;
        while(br.ready())
        {
        	String str= br.readLine();
        	if(str.equals("[")||str.equals("]"))
            {
                continue;
            }
        	if(str.endsWith(",")){
            	str=str+"\r\n";
                str=str.replace(",\r\n", "");
        	}
        	Tweet tweet;
        	try{
        		JSONObject tweetJsonObject = JSONObject.fromObject(str);
        		tweet = new Tweet(tweetJsonObject.getString("doc"));
        	}
        	catch(Exception ee){
        		System.out.println(str);
        		ee.printStackTrace();
        		continue;
        	}
    		
    		String result=tweet.location;
    		if(result!=null){
    			System.out.println(result);
    			Util.append2File(Config.testOutput, "["+result.replace(":", ",")+"],");
    		}
    		//break;
        }
        System.out.println("Total Count:"+count);
        br.close();
        

		long time = System.currentTimeMillis() - start;
		System.out.println("Time(millisec):"+time);
	}
	
	public static void runCommon() throws Exception{
		long start = System.currentTimeMillis();
		BufferedReader br = new BufferedReader(new FileReader(Config.inputTwetter));//data/bigTwitter.json
		int count=0;
        while(br.ready())
        {
        	String str= br.readLine();
        	if(str.equals("[")||str.equals("]"))
            {
                continue;
            }
        	if(str.endsWith(",")){
            	str=str+"\r\n";
                str=str.replace(",\r\n", "");
        	}
        	Tweet tweet;
        	try{
        		tweet = new Tweet(str);
        	}
        	catch(Exception ee){
        		ee.printStackTrace();
        		continue;
        	}
    		//System.out.println(tweet);
    		Process pro=new Process();
    		String sentiment = Scoring.getScore(tweet.text);
    		if(sentiment.equals(""))
    		{
    			continue;
    		}
    		String result = pro.processMealSentence(tweet.text, tweet.id, sentiment, tweet.time, tweet.location);
    		if(result!=null){
    			System.out.println(count+": "+result);
    			Util.append2File(Config.arffOutPath, result);
    			count++;
    		}
    		//break;
        }
        System.out.println("Total Count:"+count);
        br.close();
        

		long time = System.currentTimeMillis() - start;
		System.out.println("Time(millisec):"+time);
	}
	
	public static void runMedia() throws Exception{
		long start = System.currentTimeMillis();
		BufferedReader br = new BufferedReader(new FileReader(Config.inputTwetter));//data/bigTwitter.json
		int count=0;
        while(br.ready())
        {
        	String str= br.readLine();
        	if(str.equals("[")||str.equals("]"))
            {
                continue;
            }
        	if(str.endsWith(",")){
            	str=str+"\r\n";
                str=str.replace(",\r\n", "");
        	}
        	Tweet tweet;
        	try{
        		tweet = new Tweet(str);
        	}
        	catch(Exception ee){
        		ee.printStackTrace();
        		continue;
        	}
        	if(tweet.media_url==null){
        		continue;
        	}
        	else{
        		//System.out.println(tweet.media_url);
        	}
    		//System.out.println(tweet);
    		Process pro=new Process();
    		String sentiment = Scoring.getScore(tweet.text);
    		if(sentiment.equals(""))
    		{
    			continue;
    		}
    		String result = pro.processMealSentence(tweet.text, tweet.id, sentiment, tweet.time, tweet.location);
    		if(result!=null){
    			System.out.println(count+": "+tweet.media_url);
    			Util.append2File(Config.mediaUrlOutput, tweet.media_url);
    			count++;
    		}
    		//break;
        }
        System.out.println("Total Count:"+count);
        br.close();
        

		long time = System.currentTimeMillis() - start;
		System.out.println("Time(millisec):"+time);
	}

}
