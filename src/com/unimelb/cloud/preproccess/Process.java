package com.unimelb.cloud.preproccess;

import com.unimelb.cloud.config.Config;

public class Process {
	
	public Process(){
		
	}
	
	public String processMealSentence(String sentence, String id, String sentiment, String time, String location){
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		
		//1.Add sentiment result to result
		sb.append(',');
		sb.append(sentiment2Value(sentiment));
		
		//4.5 tell if it contains meal
		String[] mealHashtag={"breakfast","brunch","lunch","dinner","supper"};//Remember to delete hastag!
		String meal="";
		for(int i=0;i<mealHashtag.length;i++){
			if(sentence.contains(mealHashtag[i])){
				meal=mealHashtag[i].replace("#", "").trim();
				break;
			}
		}
		if(meal.equals("")){
			return null;
		}
		else{
			sb.append(',');
			sb.append(meal);
		}
		
		//2.Add time and location to result
		//sb.append(',');
		//sb.append(time);
		//sb.append(',');
		//sb.append(Config.city);
		
		//3.dic = loadDic();  Read dic to String[]
		DicLoader dl=new DicLoader(Config.dic);
		dl.loadDic();
		//4.Sentence assign to dic and return a 0101010 list
		//System.out.println(dl.DIC);
		int[] arr=new int[dl.DIC.length];
		boolean flag = false;
		for(int i=0;i<dl.DIC.length;i++){
			sb.append(',');
			if(sentence.contains(dl.DIC[i])){
				arr[i]=1;
				flag=true;
				//System.out.println(sentence);
				//System.out.println(dl.DIC[i]);
			}
			sb.append(arr[i]);
		}
		if(flag==false){
			return null;
		}
		
		
		
		//5.return data string
		return sb.toString();
	}
	
	public String processSentence(String sentence, String id, String sentiment, String time, String location){
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		
		//1.Add sentiment result to result
		sb.append(',');
		sb.append(sentiment2Value(sentiment));
		
		//2.Add time and location to result
		//sb.append(',');
		//sb.append(time);
		sb.append(',');
		sb.append(Config.city);
		
		//3.dic = loadDic();  Read dic to String[]
		DicLoader dl=new DicLoader(Config.dic);
		dl.loadDic();
		//4.Sentence assign to dic and return a 0101010 list
		//System.out.println(dl.DIC);
		int[] arr=new int[dl.DIC.length];
		boolean flag = false;
		for(int i=0;i<dl.DIC.length;i++){
			sb.append(',');
			if(sentence.contains(dl.DIC[i])){
				arr[i]=1;
				flag=true;
				//System.out.println(sentence);
				//System.out.println(dl.DIC[i]);
			}
			sb.append(arr[i]);
		}
		if(flag==false){
			return null;
		}
		
		//5.return data string
		return sb.toString();
	}
	
	public String processSentenceTraining(String sentence, String id, String sentiment, String time, String location){
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		
		//1.Add sentiment result to result
		sb.append(',');
		sb.append(sentiment);
		
		//2.Add time and location to result
		//sb.append(',');
		//sb.append(time);
		//sb.append(',');
		//sb.append(Config.city);
		
		//3.dic = loadDic();  Read dic to String[]
		DicLoader dl=new DicLoader(Config.dic);
		dl.loadDic();
		//4.Sentence assign to dic and return a 0101010 list
		//System.out.println(dl.DIC);
		int[] arr=new int[dl.DIC.length];
		boolean flag = false;
		for(int i=0;i<dl.DIC.length;i++){
			sb.append(',');
			if(sentence.contains(dl.DIC[i])){
				arr[i]=1;
				flag=true;
				//System.out.println(sentence);
				//System.out.println(dl.DIC[i]);
			}
			sb.append(arr[i]);
		}
		if(flag==false){
			return null;
		}
		
		//5.return data string
		return sb.toString();
	}
	
	
	public String processPic(String[] tags, String id){
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		
		
		//TODO:1.dic = loadDic();  Read dic to String[]
		DicLoader dl=new DicLoader(Config.dic);
		dl.loadDic();
		//TODO:4.Sentence assign to dic and return a 0101010 list
		StringBuilder tagSb = new StringBuilder();
		for(int i=0;i<tags.length;i++){
			tagSb.append(tags[i]);
			tagSb.append(' ');
		}
		String tagStr = tagSb.toString();
		
		int[] arr=new int[dl.DIC.length];
		for(int i=0;i<dl.DIC.length;i++){
			sb.append(',');
			if(tagStr.contains(dl.DIC[i])){
				arr[i]=1;
			}
			sb.append(arr[i]);
		}
		
		//TODO:5.return data string
		return sb.toString();
		
	}
	
	public static int sentiment2Value(String emotion){
		//System.out.println(emotion);
		if(emotion==null){
			return 0;
		}
		String[] sents={"Affection","Cheerfulness","Optimism","Astonishment","Embarrasment","Outrage","Disgust","Depressed","Worry"};
		int[] vals={5,4,3,2,1,-1,-1,-1,-1};
		for(int i=0;i<sents.length;i++){
			if(emotion.equals(sents[i])){
				return vals[i];
			}
		}		
		return 0;
	}
}
