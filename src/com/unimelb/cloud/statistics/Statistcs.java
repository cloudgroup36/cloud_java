package com.unimelb.cloud.statistics;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Statistcs {
	static String[] fooddic = readfooddic("data/FoodDic3Sorted.txt");
	static final int TWITTER_NUM = 78363;
	static final int INFO_COL = 321;
	
	public static String[] readfooddic(String filepath){
		int row = 0;
		String[] fooddic = new String[INFO_COL - 2];
		Scanner readfile = null;
		File f = new File(filepath);
		try {
			readfile = new Scanner(f);
			while (readfile.hasNextLine()) {
				String getString;
				getString = readfile.nextLine();
				fooddic[row] = getString;
				row ++;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		readfile.close();
		return fooddic;
	}
	
	/*read food table from file*/
	public static String[][] readfoodtable(String filepath) throws FileNotFoundException{
		int row = 0;
		String[][] foodtable = new String[TWITTER_NUM][INFO_COL];
		Scanner readfile = null;
		File f = new File(filepath);
		readfile = new Scanner(f);
		while (readfile.hasNextLine()) {
			String getString;
			getString = readfile.nextLine();
			String[] array = getString.split(",");
			for(int col = 0; col < INFO_COL; col ++){
				foodtable[row][col] = array[col + 1];
			}
			row ++;
		}
		readfile.close();
//		for(int i = 0; i < TWITTER_NUM; i++){
//			for(int j = 0; j < INFO_COL; j++){
//				System.out.print(foodtable[i][j] + " ");	
//			}
//			System.out.println();
//		}
		return foodtable;
	}
	
	/*read food table from file*/
	public static String[][] readmealtable(String filepath) throws FileNotFoundException{
		int row = 0;
		String[][] mealtable = new String[724][2];
		Scanner readfile = null;
		File f = new File(filepath);
		readfile = new Scanner(f);
		while (readfile.hasNextLine()) {
			String getString;
			getString = readfile.nextLine();
			String[] array = getString.split(",");
			for(int col = 0; col < 2; col ++){
				mealtable[row][col] = array[col + 1];
			}
			row ++;
		}
		readfile.close();
		return mealtable;
	}
	
	/*Sort hashmap by descending emotion index */
	public static LinkedHashMap<Integer, Double> sortbyemotion(HashMap<Integer, Double> foodlist){
	    Comparator<HashMap.Entry<Integer, Double>> valueComparator = new Comparator<HashMap.Entry<Integer, Double>>() {
			@Override
			public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
				// TODO Auto-generated method stub
				return Double.compare(o2.getValue(),o1.getValue());
			}
	    };
	    List<HashMap.Entry<Integer, Double>> list = new ArrayList<HashMap.Entry<Integer, Double>>(foodlist.entrySet());
	    Collections.sort(list, valueComparator);
	    
	    LinkedHashMap<Integer, Double> sortedfoodlist = new LinkedHashMap<Integer, Double>();
		for(int i = 0; i < list.size(); i ++) {
			Map.Entry<Integer, Double> x = list.get(i);
			sortedfoodlist.put(x.getKey(), x.getValue());
		}
		return sortedfoodlist;
	}
	
	/*Q1. What kinds of food make people happy/sad? Relate to health condition of users*/
	public static void foodemotion() throws FileNotFoundException{
		HashMap<Integer, Double> foodlist = new HashMap<>();
		String[][] foodtable = new String[TWITTER_NUM][INFO_COL];
		int[] foodcount = new int[INFO_COL - 2];
		foodtable = readfoodtable("data/data_all.txt");
		for(int col = 2; col < INFO_COL; col ++){
			double totalemotion = 0;
			for(int row = 0; row < TWITTER_NUM; row ++){
				int appear = Integer.parseInt(foodtable[row][col]);
				int currentemotion = Integer.parseInt(foodtable[row][0]);
				if(appear == 1){
					totalemotion += currentemotion;
					foodcount[col - 2] += 1;
				}
			}
			if(foodcount[col - 2] > 50){
				foodlist.put(col - 2, totalemotion / foodcount[col - 2]);
				//System.out.println(totalemotion / foodcount[col - 2]);
			}	
//			else
//				foodlist.put(col - 2, 0.0);
		}
		LinkedHashMap<Integer, Double> sortedfoodlist = new LinkedHashMap<Integer, Double>();
		sortedfoodlist = sortbyemotion(foodlist);
		JsonArray food_emotion = new JsonArray();
		
		for (Integer key : sortedfoodlist.keySet()) {
			JsonObject food = new JsonObject();
			food.addProperty("foodID", key + 1);
			food.addProperty("foodName", fooddic[key]);
			food.addProperty("EmotionIndex", foodlist.get(key));
			food_emotion.add(food);		
		}
		System.out.println(food_emotion);
	}
	
	
	/*Q2. What kinds of food make Sydney/Melbourne/... people happy/sad*/
	public static void cityfood() throws FileNotFoundException{
		HashMap<Integer, Double> mel_food = new HashMap<>();
		HashMap<Integer, Double> syd_food = new HashMap<>();
		HashMap<Integer, Double> bri_food = new HashMap<>();
		String[][] foodtable = new String[TWITTER_NUM][INFO_COL];
		foodtable = readfoodtable("data/data_all.txt");
		int[] mel_count = new int[INFO_COL - 2];
		int[] syd_count = new int[INFO_COL - 2];
		int[] bri_count = new int[INFO_COL - 2];
		for(int col = 2; col < INFO_COL; col ++){
			double mel_emotion = 0;
			double syd_emotion = 0;
			double bri_emotion = 0;
			for(int row = 0; row < TWITTER_NUM; row ++){
				int appear = Integer.parseInt(foodtable[row][col]);
				int currentemotion = Integer.parseInt(foodtable[row][0]);
				if(appear == 1){
					if(foodtable[row][1].equals("Melbourne")){
						mel_emotion += currentemotion;  //4000
						mel_count[col - 2] += 1;
					}
					else if(foodtable[row][1].equals("Sydney")){
						syd_emotion += currentemotion;  //45000
						syd_count[col - 2] += 1;
					}
					else{
						bri_emotion += currentemotion;  //29000
						bri_count[col - 2] += 1;
					}
				}
			}
			if(mel_count[col - 2] > 5)
				mel_food.put(col - 2, mel_emotion / mel_count[col - 2]);
			if(syd_count[col - 2] > 30)
				syd_food.put(col - 2, syd_emotion /syd_count[col - 2]);
			if(bri_count[col - 2] > 10)
				bri_food.put(col - 2, bri_emotion / bri_count[col - 2]);
		}
		
		LinkedHashMap<Integer, Double> sorted_mel_food = new LinkedHashMap<Integer, Double>();
		LinkedHashMap<Integer, Double> sorted_syd_food = new LinkedHashMap<Integer, Double>();
		LinkedHashMap<Integer, Double> sorted_bri_food = new LinkedHashMap<Integer, Double>();
		sorted_mel_food = sortbyemotion(mel_food);
		sorted_syd_food = sortbyemotion(syd_food);
		sorted_bri_food = sortbyemotion(bri_food);
		
		JsonArray mel_foodemotion = new JsonArray();
		JsonObject mel_food_emotion = new JsonObject();
		mel_food_emotion.addProperty("City", "Melbourne");
		for (Integer key : sorted_mel_food.keySet()) {
			JsonObject food = new JsonObject();
			food.addProperty("foodID", key + 1);
			food.addProperty("foodName", fooddic[key]);
			food.addProperty("EmotionIndex", sorted_mel_food.get(key));
			mel_foodemotion.add(food);		
		}
		mel_food_emotion.add("Food", mel_foodemotion);
		
		JsonArray syd_foodemotion = new JsonArray();
		JsonObject syd_food_emotion = new JsonObject();
		syd_food_emotion.addProperty("City", "Sydney");
		for (Integer key : sorted_syd_food.keySet()) {
			JsonObject food = new JsonObject();
			food.addProperty("foodID", key + 1);
			food.addProperty("foodName", fooddic[key]);
			food.addProperty("EmotionIndex", sorted_syd_food.get(key));
			syd_foodemotion.add(food);		
		}
		syd_food_emotion.add("Food", syd_foodemotion);
		
		JsonArray bri_foodemotion = new JsonArray();
		JsonObject bri_food_emotion = new JsonObject();
		bri_food_emotion.addProperty("City", "Brisbane");
		for (Integer key : sorted_bri_food.keySet()) {
			JsonObject food = new JsonObject();
			food.addProperty("foodID", key + 1);
			food.addProperty("foodName", fooddic[key]);
			food.addProperty("EmotionIndex", sorted_bri_food.get(key));
			bri_foodemotion.add(food);		
		}
		bri_food_emotion.add("Food", bri_foodemotion);
		
		System.out.println(mel_food_emotion);
		System.out.println(syd_food_emotion);
		System.out.println(bri_food_emotion);
	}
	
	/*Q5. Meals (breakfast/brunch/lunch/supper/dinner) related with type of emotions*/
	public static void mealfood() throws FileNotFoundException{
		int bref = 0, brun = 0, lun = 0, sup = 0, din = 0;
		int brefnum = 0, brunnum = 0, lunnum = 0, supnum = 0, dinnum = 0;
		String[][] mealtable = new String[724][2];
		mealtable = readfoodtable("data/meal_data_all.txt");
		for(int row = 0; row < 724; row ++){
			String meal = mealtable[row][1];
			switch(meal){
			case "breakfast":
				brefnum ++;
				bref += Integer.parseInt(mealtable[row][0]);
				break;
			case "brunch":
				brunnum ++;
				brun += Integer.parseInt(mealtable[row][0]);
				break;
			case "lunch":
				lunnum ++;
				lun += Integer.parseInt(mealtable[row][0]);
				break;
			case "dinner":
				dinnum ++;
				din += Integer.parseInt(mealtable[row][0]);
				break;
			case "supper":
				supnum ++;
				sup += Integer.parseInt(mealtable[row][0]);
				break;
			}
		}
		JsonObject meal_emotion = new JsonObject();
		meal_emotion.addProperty("breakfast", (double)bref / brefnum);
		meal_emotion.addProperty("brunch", (double)brun / brunnum);
		meal_emotion.addProperty("lunch", (double)lun / lunnum);
		meal_emotion.addProperty("dinner", (double)din / dinnum);
		meal_emotion.addProperty("supper", (double)sup / supnum);
		System.out.println(meal_emotion);
	}
	
	
	public static void main(String[] args) throws FileNotFoundException{
		System.out.println("What kinds of food make people happy/sad?");
		foodemotion();
		System.out.println();
		System.out.println("What kinds of food make Sydney/Melbourne/... people happy/sad");
		cityfood();
		System.out.println();
		System.out.println("Meals (breakfast/brunch/lunch/supper/dinner) related with type of emotions");
		mealfood();
	}
}
