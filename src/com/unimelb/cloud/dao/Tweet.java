package com.unimelb.cloud.dao;

/*import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
*/
import net.sf.json.*;
public class Tweet {
	public String id;
	public String text;
	public String media_url;
	public String location;
	public String time;
	public Tweet(String json) {
		super();

    	//System.out.println(json);
		/*JSONParser parser = new JSONParser();
		try {
			JSONObject tweetJsonObject = (JSONObject) parser.parse(json);
			this.id =tweetJsonObject.get("id").toString() ;
			this.text = tweetJsonObject.get("text").toString().replace("\n", " ");
			Object media = tweetJsonObject.get("entities").get("media");
			if(media!=null){
				this.media_url = tweetJsonObject.getJSONObject("entities").getJSONArray("media").getJSONObject(0).getString("media_url") ;
			}
			this.location = tweetJsonObject.getJSONObject("coordinates").getString("coordinates").replace("[", "").replace("]", "").replace(",", ":");
			this.time = tweetJsonObject.getString("created_at");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		JSONObject tweetJsonObject = JSONObject.fromObject(json);
		//json text/id/    json entities media [0] media_url
		this.id =tweetJsonObject.getString("id") ;
		//System.out.println(this.id);
		this.text = tweetJsonObject.getString("text").replace("\n", " ");
		Object media = tweetJsonObject.getJSONObject("entities").get("media");
		if(media!=null){
			this.media_url = tweetJsonObject.getJSONObject("entities").getJSONArray("media").getJSONObject(0).getString("media_url") ;
		}
		String coordinates = tweetJsonObject.getString("coordinates");
		//System.out.println(coordinates);
		if(!coordinates.equals("null")){
			this.location = tweetJsonObject.getJSONObject("coordinates").getString("coordinates").replace("[", "").replace("]", "").replace(",", ":");
		}
		this.time = tweetJsonObject.getString("created_at");
		
	}
	
	public Tweet(String id, String text, String media_url, String location,
			String time) {
		super();
		this.id = id;
		this.text = text;
		this.media_url = media_url;
		this.location = location;
		this.time = time;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getMedia_url() {
		return media_url;
	}
	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "Tweet [id=" + id + ", text=" + text + ", media_url="
				+ media_url + ", location=" + location + ", time=" + time + "]";
	}
	
	

}
