package com.unimelb.cloud.computervision;
import java.io.BufferedReader;
import java.net.URI;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.unimelb.cloud.config.Config;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.io.*;
public class ComputerVision {
	public static String computetags(String url){
		HttpClient httpclient = new DefaultHttpClient();
		String tags = "";
		try{
			URIBuilder builder = new URIBuilder("https://westus.api.cognitive.microsoft.com/vision/v1.0/tag");

	        URI uri = builder.build();
	        HttpPost request = new HttpPost(uri);

	        // Request headers - replace this example key with your valid subscription key.
	        request.setHeader("Content-Type", "application/json");
	        request.setHeader("Ocp-Apim-Subscription-Key", "c0c0657ca65149a081787ba1882ac6f4");

	        // Request body. Replace the example URL with the URL for the JPEG image.
	        String picurl = "{\"url\":\"" + url + "\"}";
	        //System.out.println(picurl);
	        StringEntity reqEntity = new StringEntity(picurl);
	        request.setEntity(reqEntity);

	        HttpResponse response = httpclient.execute(request);
	        HttpEntity entity = response.getEntity();

	        if (entity != null){
	        	tags = EntityUtils.toString(entity);
	        	}
	        }catch (Exception e){
	        	System.out.println(e.getMessage());
	        	}
		//System.out.println(tags);
		return tags;		
	}
	
	public static void main(String[] args) throws Exception{
		BufferedReader br=new BufferedReader(new FileReader(Config.mediaUrlOutput));
		while(br.ready()){
			String line=br.readLine();
			//System.out.println(line);
			String lineResult=getWordsFromURL(line);
			Thread.sleep(5000);
			System.out.println(lineResult);
		}
		br.close();
		//String line=getWordsFromURL("http://www.omahasteaks.com/gifs/990x594/fi004.jpg");
		//System.out.println(line);
		/*// TODO Auto-generated method stub
		String tags = computetags("http://www.omahasteaks.com/gifs/990x594/fi004.jpg");
		String tags = "{\"tags\":[{\"name\":\"cake\",\"confidence\":0.99999582767486572},"
				+ "{\"name\":\"chocolate\",\"confidence\":0.99988973140716553},"
						+ "{\"name\":\"food\",\"confidence\":0.991118848323822},"
						+ "{\"name\":\"piece\",\"confidence\":0.97822684049606323},"
						+ "{\"name\":\"plate\",\"confidence\":0.94466018676757813},"
						+ "{\"name\":\"ice\",\"confidence\":0.910990297794342},"
						+ "{\"name\":\"cream\",\"confidence\":0.88919734954833984},"
						+ "{\"name\":\"slice\",\"confidence\":0.88755142688751221},"
						+ "{\"name\":\"dessert\",\"confidence\":0.734363317489624},"
						+ "{\"name\":\"desert\",\"confidence\":0.63612514734268188},"
						+ "{\"name\":\"plant\",\"confidence\":0.54377830028533936},"
						+ "{\"name\":\"square\",\"confidence\":0.43289849162101746},"
						+ "{\"name\":\"eaten\",\"confidence\":0.418596476316452},"
						+ "{\"name\":\"decorated\",\"confidence\":0.35598805546760559}],"
						+ "\"requestId\":\"d0bd4956-a307-4ada-8d34-9dcf1f149006\","
						+ "\"metadata\":{\"width\":1024,\"height\":667,\"format\":\"Jpeg\"}}";
		int start = tags.indexOf('[');
		int end  = tags.indexOf(']');
		String tags_arr = tags.substring(start, end + 1);
		
		JSONArray json = JSONArray.fromObject(tags_arr);
		int length = json.size();
		HashMap<String , String> map = new HashMap<String , String>();
		
		for (int i = 0; i < length; i++) {
			JSONObject jsonObj = json.getJSONObject(i);
			map.put(jsonObj.get("name").toString(), jsonObj.get("confidence").toString());
		}
		System.out.println(map);*/
	}
	
	public static String getWordsFromURL(String url){
		String tags = computetags(url);
		int start = tags.indexOf('[');
		int end  = tags.indexOf(']');
		String tags_arr;
		try{
			tags_arr = tags.substring(start, end + 1);
		}
		catch(Exception ee){
			System.out.println(tags);
			return url+"+";
		}
		
		
		JSONArray json = JSONArray.fromObject(tags_arr);
		int length = json.size();
		HashMap<String , String> map = new HashMap<String , String>();
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < length; i++) {
			if(i!=0){
				result.append('+');
			}
			JSONObject jsonObj = json.getJSONObject(i);
			map.put(jsonObj.get("name").toString(), jsonObj.get("confidence").toString());
			result.append(jsonObj.get("name").toString());
		}
		//System.out.println(map);
		return url+"+"+result.toString().trim();
	}
}
